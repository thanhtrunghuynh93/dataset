import os
import pdb

def rename_files(path):
    if not os.path.isdir(path):
        return
    files = os.listdir(path)

    contains = ["-G.json", "-id_map.json", "-feats.npy"]
    replaces = ["G.json", "id2idx.json", "feats.npy"]
    for file in files:
        if os.path.isdir(os.path.join(path,file)):
            rename_files(os.path.join(path, file))
        else:
            for i, c in enumerate(contains):
                if c in file:
                    # print('\nxxxxxxxxxx\n')
                    # print('\n'.join([c,os.path.join(path, file), os.path.join(path, replaces[i])]))
                    # pass
                    os.rename(os.path.join(path, file), os.path.join(path, replaces[i]))


rename_files("graph")