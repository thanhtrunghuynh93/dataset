Source dataset info:
Name: watts_strogatz_graph(1000,40,0.5)
Type: Graph
Number of nodes: 1000
Number of edges: 20000
Average degree:  40.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 19778
Average degree:  39.5560
Ratio of neighbors in source are also neighbors in target: 0.9889
Ratio of same feature groundtruth: 1.0000