Source dataset info:
Name: watts_strogatz_graph(1000,20,0.5)
Type: Graph
Number of nodes: 1000
Number of edges: 10000
Average degree:  20.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 8934
Average degree:  17.8680
Ratio of neighbors in source are also neighbors in target: 0.8934
Ratio of same feature groundtruth: 1.0000