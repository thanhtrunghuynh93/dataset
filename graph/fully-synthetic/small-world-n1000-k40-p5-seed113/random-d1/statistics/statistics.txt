Source dataset info:
Name: watts_strogatz_graph(1000,40,0.5)
Type: Graph
Number of nodes: 1000
Number of edges: 20000
Average degree:  40.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 17978
Average degree:  35.9560
Ratio of neighbors in source are also neighbors in target: 0.8989
Ratio of same feature groundtruth: 1.0000