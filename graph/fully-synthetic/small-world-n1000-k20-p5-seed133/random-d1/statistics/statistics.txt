Source dataset info:
Name: watts_strogatz_graph(1000,20,0.5)
Type: Graph
Number of nodes: 1000
Number of edges: 10000
Average degree:  20.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 9015
Average degree:  18.0300
Ratio of neighbors in source are also neighbors in target: 0.9015
Ratio of same feature groundtruth: 1.0000