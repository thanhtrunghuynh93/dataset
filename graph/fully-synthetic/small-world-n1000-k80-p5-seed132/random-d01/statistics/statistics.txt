Source dataset info:
Name: watts_strogatz_graph(1000,80,0.5)
Type: Graph
Number of nodes: 1000
Number of edges: 40000
Average degree:  80.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 39651
Average degree:  79.3020
Ratio of neighbors in source are also neighbors in target: 0.9913
Ratio of same feature groundtruth: 1.0000