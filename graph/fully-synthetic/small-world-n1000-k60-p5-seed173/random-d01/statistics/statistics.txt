Source dataset info:
Name: watts_strogatz_graph(1000,60,0.5)
Type: Graph
Number of nodes: 1000
Number of edges: 30000
Average degree:  60.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 29730
Average degree:  59.4600
Ratio of neighbors in source are also neighbors in target: 0.9910
Ratio of same feature groundtruth: 1.0000