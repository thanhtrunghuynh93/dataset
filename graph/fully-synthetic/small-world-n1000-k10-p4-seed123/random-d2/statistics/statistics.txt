Source dataset info:
Name: watts_strogatz_graph(1000,10,0.4)
Type: Graph
Number of nodes: 1000
Number of edges: 5000
Average degree:  10.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 3973
Average degree:   7.9460
Ratio of neighbors in source are also neighbors in target: 0.7946
Ratio of same feature groundtruth: 1.0000