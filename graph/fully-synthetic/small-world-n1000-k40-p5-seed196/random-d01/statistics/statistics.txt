Source dataset info:
Name: watts_strogatz_graph(1000,40,0.5)
Type: Graph
Number of nodes: 1000
Number of edges: 20000
Average degree:  40.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 19783
Average degree:  39.5660
Ratio of neighbors in source are also neighbors in target: 0.9891
Ratio of same feature groundtruth: 1.0000