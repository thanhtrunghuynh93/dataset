Source dataset info:
Name: watts_strogatz_graph(1000,20,0.5)
Type: Graph
Number of nodes: 1000
Number of edges: 10000
Average degree:  20.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 9902
Average degree:  19.8040
Ratio of neighbors in source are also neighbors in target: 0.9902
Ratio of same feature groundtruth: 1.0000