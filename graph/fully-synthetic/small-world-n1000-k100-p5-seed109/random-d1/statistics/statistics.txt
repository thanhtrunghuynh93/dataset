Source dataset info:
Name: watts_strogatz_graph(1000,100,0.5)
Type: Graph
Number of nodes: 1000
Number of edges: 50000
Average degree: 100.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 44883
Average degree:  89.7660
Ratio of neighbors in source are also neighbors in target: 0.8977
Ratio of same feature groundtruth: 1.0000