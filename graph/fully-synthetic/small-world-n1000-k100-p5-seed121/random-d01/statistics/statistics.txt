Source dataset info:
Name: watts_strogatz_graph(1000,100,0.5)
Type: Graph
Number of nodes: 1000
Number of edges: 50000
Average degree: 100.0000
Target dataset info:
Name: 
Type: Graph
Number of nodes: 1000
Number of edges: 49507
Average degree:  99.0140
Ratio of neighbors in source are also neighbors in target: 0.9901
Ratio of same feature groundtruth: 1.0000